package test_web1

import "time"

type Todo struct {
	ID		  int		`json:"id"`
	Name      string 	`json:"name"`
	Completed bool 		`json:"completed"`
	Due       time.Time `json:"due"`
}
