//	By using test_web1 SDK you can get access to todo
//	objects via Client.
package test_web1

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

//	Using Client struct you can fetch current todo
//	objects and perform CRUD operations on them

type retryer interface {
	retryRules(*http.Response) time.Duration
	maxRetries() int
	shouldRetry(*http.Response) bool
}

type Client struct {
	retryer
	url string
	maxPageSize int
}

type concurrentTodo struct {
	todo Todo
	err error
}

var noContent = errors.New("no content")

//	initiates and returns a new sdk client to use
func NewClient() *Client {
	return &Client{url: "http://localhost:8080", maxPageSize: 100}
}

func (c *Client) checkForMaxPageSize(pageSize int) error {
	if pageSize > c.maxPageSize {
		return fmt.Errorf("page size %d exceeds %d", pageSize, c.maxPageSize)
	}
	return nil
}

func (*Client) retryRules(*http.Response) time.Duration {
	return time.Second * 3
}

func (*Client) maxRetries() int {
	return 10
}

//func (*Client) shouldRetry(r *http.Response) bool {
//	allowedMethods := [3]int{200, 201, 204}
//	for _, allowedMethod := range allowedMethods {
//		if r.StatusCode == allowedMethod {
//			return false
//		}
//	}
//	return true
//}

func (c *Client) retryGet(path string) (*http.Response, error) {
	for numOfRetries := 0; numOfRetries < c.maxRetries(); numOfRetries++{
		fullURL := c.url + "/" + path
		var response *http.Response
		var err error
		response, err = http.Get(fullURL)
		if err != nil{
			waitFor := c.retryRules(response)
			time.Sleep(waitFor)
			continue
		}
		return response, nil
	}
	return nil, fmt.Errorf("retry to get todo with path %s exceeded %d", path, c.maxRetries())
}

func (c *Client) GetTodos(pageSize int, pageNumber int) ([]Todo, error){
	if err := c.checkForMaxPageSize(pageSize); err != nil {
		return nil, err
	}
	firstTodoIndex := pageSize * pageNumber
	lastTodoIndex := pageSize * (pageNumber + 1) - 1
	allTodos := make([]Todo, 0, pageSize)
	channs := make([]chan concurrentTodo, pageSize)
	for index := range channs{
		channs[index] = make(chan concurrentTodo)
	}

	for todoIndex := firstTodoIndex; todoIndex <= lastTodoIndex; todoIndex++{
		channIndex := todoIndex - firstTodoIndex
		go func(todoId int, outputChan chan concurrentTodo) {
			todo, err := c.GetTodo(todoId)
			outputChan<- concurrentTodo{todo, err}
		}(todoIndex, channs[channIndex])
	}
	for _, outputChan := range channs {
		output := <-outputChan
		newTodo, err := output.todo, output.err
		if err != nil {
			if err == noContent {
				continue
			}
			return nil, err
		}
		allTodos = append(allTodos, newTodo)
	}
	return allTodos, nil
}

func (c *Client) GetTodo(todoId int) (Todo, error) {
	path := fmt.Sprintf("todos/%d/", todoId)
	response, err := c.retryGet(path)
	if err != nil {
		return Todo{}, err
	}
	defer response.Body.Close()
	if response.StatusCode == 404 {
		return Todo{}, noContent
	}

	todo := Todo{}
	dec := json.NewDecoder(response.Body)
	dec.DisallowUnknownFields()
	err = dec.Decode(&todo)
	if err != nil {
		return Todo{}, err
	}
	return todo, nil
}