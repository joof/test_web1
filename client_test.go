package test_web1

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gopkg.in/h2non/gock.v1"
	"net/http"
	"regexp"
	"testing"
	"time"
)

var expectedTodos = []Todo {
	{ID: 1, Name: "Write presentation 1", Completed: false},
	{ID: 2, Name: "Write presentation 2", Completed: true,
		Due: time.Date(2020, time.December, 06, 20, 10, 11, 0, time.UTC)},
	{ID: 3, Name: "Write presentation 3", Completed: false},
	{ID: 4, Name: "Write presentation 4", Completed: true,
		Due: time.Date(2016, time.November, 16, 0, 5, 0, 0, time.Local)},
}


func setUpMockServer() {
	for _, expected := range expectedTodos {
		expectedJson, _ := json.Marshal(expected)
		gock.New("http://localhost:8080").
			Get(fmt.Sprintf("/todos/%d/", expected.ID)).
			Reply(200).
			JSON(expectedJson)
	}
	gock.New("http://localhost:8080").
		Get("/todos/").
		Filter(func (req *http.Request) bool {
			for _, expected := range expectedTodos {
				if matched, _ := regexp.MatchString(fmt.Sprintf("/todos/%v", expected.ID), req.URL.Path); matched {
					return false
				}
			}
			return true
		}).
		Reply(404)
}

func TestClient_GetTodos(t *testing.T) {
	setUpMockServer()
	defer gock.Off()

	c := NewClient()
	pageSize := 5
	pageNumber := 0
	todos, err := c.GetTodos(pageSize, pageNumber)
	require.Nil(t, err)
	assert.Equal(t, expectedTodos, todos)
}

func TestClient_GetTodosPageSizeTooBig(t *testing.T) {
	setUpMockServer()
	defer gock.Off()

	c := NewClient()
	pageSize := c.maxPageSize + 1
	pageNumber := 0
	_, err := c.GetTodos(pageSize, pageNumber)
	assert.NotNil(t, err)
}

func TestRetryGet(t *testing.T) {
	setUpMockServer()
	defer gock.Off()

	c := NewClient()
	response, err := c.retryGet("todos/1/")
	expectedTodo := expectedTodos[0]
	require.Nil(t, err)
	defer response.Body.Close()
	require.Nil(t, err)
	todo := new(Todo)
	dec := json.NewDecoder(response.Body)
	dec.DisallowUnknownFields()
	_ = dec.Decode(todo)
	assert.Equal(t, expectedTodo, *todo)
	response, err = c.retryGet("todos/6/")
	assert.Nil(t, err)
	defer response.Body.Close()
	assert.Equal(t, 404, response.StatusCode)
}

func TestGetTodo(t *testing.T) {
	setUpMockServer()
	defer gock.Off()

	c := NewClient()
	todo, err := c.GetTodo(1)
	expectedTodo := expectedTodos[0]
	require.Nil(t, err)
	assert.Equal(t, expectedTodo, todo)
	_, err = c.GetTodo(6)
	assert.NotNil(t, err)
}

func ExampleClient_GetTodos() {
	setUpMockServer()
	defer gock.Off()

	c := NewClient()
	pageSize := 4
	pageNumber := 0
	todos, _ := c.GetTodos(pageSize, pageNumber)
	fmt.Println(todos)
	// Output: [{1 Write presentation 1 false 0001-01-01 00:00:00 +0000 UTC} {2 Write presentation 2 true 2020-12-06 20:10:11 +0000 UTC} {3 Write presentation 3 false 0001-01-01 00:00:00 +0000 UTC}]
}

func BenchmarkClient_GetTodos(b *testing.B) {
	gock.Off()
	c := NewClient()
	pageSize := 5
	pageNumber := 0
	b.ResetTimer()
	for i := 0; i < b.N; i++{
		setUpMockServer()
		_, _ = c.GetTodos(pageSize, pageNumber)
	}
}